FROM golang:1.20

# gcc for cgo
RUN dpkg --add-architecture arm64 && \
    apt-get update && apt-get install -y \
        build-essential \
        autotools-dev \
        libssl-dev \
        dh-autoreconf \
        opensc \
        opensc-pkcs11 \
        g++ \
        gcc \
        libc6-dev \
        make \
        cmake \
        curl \
        libpcre3-dev \
        git \
        wget \
        sudo \
        gcc-arm-linux-gnueabihf \
        g++-arm-linux-gnueabihf \
        libusb++-dev \
        libltdl-dev \
        libltdl-dev:arm64 \
        libssh2-1 \
        zip \
        libgl1-mesa-dev \
        xorg-dev sudo

# Add the google cloud sdk. This will allow child containers to push to google cloud repositories
RUN echo "deb http://packages.cloud.google.com/apt cloud-sdk-bionic main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && apt-get install google-cloud-sdk -y

# So that we can build kathrein stuff
COPY libReaderLib.so /usr/local/lib

RUN mkdir /tmp/swig/
COPY swig.tar.gz /tmp/swig/
RUN tar -C /tmp/swig/ --strip-components=1 -xzf /tmp/swig/swig.tar.gz \
	&& cd /tmp/swig/ \
	&& ./configure \
	&& make \
	&& make install

RUN mkdir -p /home/developer/ && \
    echo "developer:x:1000:1000:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:1000:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown developer:developer -R /home/developer

USER developer
WORKDIR /home/developer/
ENV PATH /usr/local/go/bin/:$PATH
CMD /bin/bash
